package com.company;

import model.Author;
import model.Book;
import org.apache.commons.lang3.time.StopWatch;
import parsers.BookJSONParser;
import parsers.BookYAMLParser;

public class Main {

    public static final int LOOPS = 50;

    public static void main(String[] args) {
        String jsonFilename = "yaml_files/parsing/file.json";

        String yamlFilename = "yaml_files/parsing/file.yml";
        String yamlErrorFilename = "yaml_files/error_files/errorfile";
        BookJSONParser jsonParser = new BookJSONParser(jsonFilename);
        jsonParser.serialize(new Book(new Author("name", "123-123-123"), 123, "dfsas"));

        BookYAMLParser yamlParser = new BookYAMLParser(yamlFilename);
        yamlParser.serialize1(new Book(new Author("name", "123-123-123"), 123, "dfsas"));

        //json deserialization
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        for (int i = 0; i < LOOPS; i++) {
            Book e = jsonParser.deserialize();
        }
        stopWatch.stop();
        System.out.println("json worked: " + stopWatch.getTime());

        stopWatch.reset();

        //yaml deserialization
        stopWatch.start();
        for (int i = 0; i < LOOPS; i++) {
            Book e;
            try {
                e = yamlParser.deserialize1();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        stopWatch.stop();
        System.out.println("yaml worked: " + stopWatch.getTime());

        for (int i = 0; i < 5; i++) {
            yamlParser.setFilename(yamlErrorFilename + i + ".yml");
            try {
                yamlParser.deserialize1();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
