package model;

import java.util.*;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public class Book {
    public int number;
    public Author author;
    public Embedded1 embedded1;
    public Embedded2 embedded2;
    public Embedded3 embedded3;
    public Embedded4 embedded4;
    public Embedded5 embedded5;
    public String name;
    public List<String> list = new ArrayList<String>();

    public ArrayList<Integer> list1;
    public List<Integer> list2;
    public List<String> list3;
    public List<String> list4;
    public List<String> list5;
    public List<Double> list6;
    public List<Double> list7;
    public List<Embedded1> list8;
    public List<Embedded4> list9;
    public Map<String, Integer> map1;
    public Map<Integer, String> map2;
    public Map<String, Integer> map3;


    public String strel1;
    public String strel2;
    public String strel3;
    public String strel4;
    public String strel5;
    public String strel6;
    public String strel7;
    public String strel8;

    public Integer intel1;
    public Integer intel2;
    public Integer intel3;
    public Integer intel4;
    public Integer intel5;
    public Integer intel6;
    public Integer intel7;

    public Character charel1;
    public Character charel2;
    public Character charel3;
    public Character charel4;
    public Character charel5;

    public Double doubleel1;
    public Double doubleel2;
    public Double doubleel3;
    public Double doubleel4;

    public Book() {
    }

    public Book(Author author, int number, String name) {
        embedded1 = new Embedded1(true);
        embedded2 = new Embedded2(true);
        embedded3 = new Embedded3(true);
        embedded4 = new Embedded4(true);
        embedded5 = new Embedded5(true);
        this.author = author;
        this.number = number;
        this.name = name;
        list.add("sadfasf");
        list.add("asdf");
        list.add("xvbxz");


        strel1 = "asdf";
        strel2 = "sdfgdfh";
        strel3 = "fghfg";
        strel4 = "cvbncvb";
        strel5 = "fghfgh";
        strel6 = "vbnfg";
        strel7 = "fgjfgjh";
        strel8 = "hfghrt";

        intel1 = 234;
        intel2 = 4536;
        intel3 = 73;
        intel4 = 345;
        intel5 = 645;
        intel6 = 844;
        intel7 = 4563;

        charel1 = 's';
        charel2 = 'g';
        charel3 = 'h';
        charel4 = 'e';
        charel5 = 'y';

        doubleel1 = 1234.2;
        doubleel2 = 7456.56;
        doubleel3 = 456.5;
        doubleel4 = 456.945;


        map1 = new HashMap<String, Integer>();
        map1.put("1", 1);
        map1.put("2", 2);
        map1.put("3", 3);
        map2 = new HashMap<Integer, String>();
        map2.put(1, "1");
        map2.put(2, "2");
        map2.put(3, "3");
        map2.put(4, "4");
        map2.put(5, "5");
        map2.put(6, "6");
        map2.put(7, "7");
        map3 = new HashMap<String, Integer>();
        map3.put("1", 1);
        map3.put("2", 2);
        map3.put("3", 3);
        map3.put("4", 4);
        map3.put("5", 5);
        map3.put("6", 6);
        list1 = new ArrayList(Arrays.asList(1, 2, 3, 4, 5));
        list2 = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4));
        list3 = new ArrayList<String>(Arrays.asList("1", "2", "3", "4"));
        list4 = new ArrayList<String>(Arrays.asList("sdfs", "sdfs", "sdfgsdf", "sgsdf"));
        list5 = new ArrayList<String>(Arrays.asList("1", "2", "3", "4", "5", "6"));
        list6 = new ArrayList<Double>(Arrays.asList(112.1, 234.12, 3342.2314, 344.456, 3245.0395, 345.6));
        list7 = new ArrayList<Double>(Arrays.asList(564.75, 564.6, 3425.6758, 6785.5678, 456.45, 3245.7546));
        list8 = new ArrayList<Embedded1>(Arrays.asList(new Embedded1(true), new Embedded1(true)));
        list9 = new ArrayList<Embedded4>(Arrays.asList(new Embedded4(true), new Embedded4(true), new Embedded4(true), new Embedded4(true)));
    }

    @Override
    public String toString() {
        return "Book{" +
                "number=" + number +
                ", author=" + author.toString() +
                ", name='" + name + '\'' +
                ", list=" + list +
                '}';
    }
}
