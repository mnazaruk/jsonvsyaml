package model;

import java.util.*;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public class Embedded3 {
    public String strel1;
    public String strel2;
    public String strel3;
    public Integer intel1;
    public Integer intel2;
    public Integer intel3;
    public Integer intel4;
    public Integer intel5;
    public Integer intel6;
    public Integer intel7;

    public ArrayList<Integer> list1;
    public List<Integer> list2;
    public List<String> list3;
    public List<Integer> list4;
    public Map<String, Integer> map1;
    public Map<String, Integer> map2;

    public Embedded3() {
    }


    public Embedded3(boolean flag) {

        list1 = new ArrayList(
                Arrays.asList(1, 2, 3, 4, 5));
        list2 = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4));
        list3 = new ArrayList<String>(Arrays.asList("1", "2", "3", "4"));
        list4 = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
        map1 = new HashMap<String, Integer>();
        map1.put("1", 1);
        map1.put("2", 2);
        map1.put("3", 3);
        map2 = new HashMap<String, Integer>();
        map2.put("1", 1);
        map2.put("2", 2);
        map2.put("3", 3);
        map2.put("4", 4);
        map2.put("5", 5);

        strel1 = "el1";
        strel2 = "el2";
        strel3 = "el3";
        intel1 = 1;
        intel2 = 2;
        intel3 = 3;
        intel4 = 4;
        intel5 = 5;
        intel6 = 6;
        intel7 = 7;
    }
}
