package model;

import java.util.*;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public class Embedded5 {
    public String strel1;
    public String strel2;
    public String strel3;
    public Integer intel1;
    public Integer intel2;
    public Integer intel3;

    ArrayList<Integer> list1;
    List<Integer> list2;
    List<String> list3;
    List<String> list4;
    List<String> list5;
    List<Integer> list6;
    Map<String, Integer> map1;
    Map<Integer, String> map2;
    Map<String, Integer> map3;

    public Embedded5() {
    }

    public Embedded5(boolean flag) {

        map1 = new HashMap<String, Integer>();
        map1.put("1", 1);
        map1.put("2", 2);
        map1.put("3", 3);
        map2 = new HashMap<Integer, String>();
        map2.put(1, "1");
        map2.put(2, "2");
        map2.put(3, "3");
        map2.put(4, "4");
        map2.put(5, "5");
        map2.put(6, "6");
        map2.put(7, "7");
        map3 = new HashMap<String, Integer>();
        map3.put("1", 1);
        map3.put("2", 2);
        map3.put("3", 3);
        map3.put("4", 4);
        map3.put("5", 5);
        map3.put("6", 6);
        list1 = new ArrayList(Arrays.asList(1, 2, 3, 4, 5));
        list2 = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4));
        list3 = new ArrayList<String>(Arrays.asList("1", "2", "3", "4"));
        list4 = new ArrayList<String>(Arrays.asList("sdfs", "sdfs", "sdfgsdf", "sgsdf"));
        list5 = new ArrayList<String>(Arrays.asList("1", "2", "3", "4", "5", "6"));
        list6 = new ArrayList<Integer>(Arrays.asList(112, 234, 3342, 344, 3245, 345));
        strel1 = "el1";
        strel2 = "el2";
        strel3 = "el3";
        intel1 = 1;
        intel2 = 2;
        intel3 = 3;
    }

    public String getStrel1() {
        return strel1;
    }

    public void setStrel1(String strel1) {
        this.strel1 = strel1;
    }

    public String getStrel2() {
        return strel2;
    }

    public void setStrel2(String strel2) {
        this.strel2 = strel2;
    }

    public String getStrel3() {
        return strel3;
    }

    public void setStrel3(String strel3) {
        this.strel3 = strel3;
    }

    public Integer getIntel1() {
        return intel1;
    }

    public void setIntel1(Integer intel1) {
        this.intel1 = intel1;
    }

    public Integer getIntel2() {
        return intel2;
    }

    public void setIntel2(Integer intel2) {
        this.intel2 = intel2;
    }

    public Integer getIntel3() {
        return intel3;
    }

    public void setIntel3(Integer intel3) {
        this.intel3 = intel3;
    }

    public ArrayList<Integer> getList1() {
        return list1;
    }

    public void setList1(ArrayList<Integer> list1) {
        this.list1 = list1;
    }

    public List<Integer> getList2() {
        return list2;
    }

    public void setList2(List<Integer> list2) {
        this.list2 = list2;
    }

    public List<String> getList3() {
        return list3;
    }

    public void setList3(List<String> list3) {
        this.list3 = list3;
    }

    public List<String> getList4() {
        return list4;
    }

    public void setList4(List<String> list4) {
        this.list4 = list4;
    }

    public List<String> getList5() {
        return list5;
    }

    public void setList5(List<String> list5) {
        this.list5 = list5;
    }

    public List<Integer> getList6() {
        return list6;
    }

    public void setList6(List<Integer> list6) {
        this.list6 = list6;
    }

    public Map<String, Integer> getMap1() {
        return map1;
    }

    public void setMap1(Map<String, Integer> map1) {
        this.map1 = map1;
    }

    public Map<Integer, String> getMap2() {
        return map2;
    }

    public void setMap2(Map<Integer, String> map2) {
        this.map2 = map2;
    }

    public Map<String, Integer> getMap3() {
        return map3;
    }

    public void setMap3(Map<String, Integer> map3) {
        this.map3 = map3;
    }
}
