package model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public class Author {

    @NotNull(message = "Author name is compulsory")
    @Size(min = 2, message = "Name is too short")
    public String name;

    @NotNull(message = "Author phone is compulsory")
    @Size(min = 5, message = "Author phone is too short")
    public String phone;

    public Author() {
    }

    public Author(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
