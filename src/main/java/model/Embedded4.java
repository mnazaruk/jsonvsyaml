package model;

import java.util.*;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public class Embedded4 {
    public String strel1;
    public String strel2;
    public String strel3;
    public String strel4;
    public String strel5;
    public String strel6;
    public String strel7;
    public Integer intel1;
    public Integer intel2;
    public Integer intel3;
    public Integer intel4;
    public Integer intel5;
    public Integer intel6;
    public Integer intel7;

    public ArrayList<Integer> list1;
    public List<String> list2;
    public Map<String, Integer> map1;

    public Embedded4() {
    }

    public Embedded4(boolean flag) {
        list1 = new ArrayList(Arrays.asList(1, 2, 3, 4, 5));
        list2 = new ArrayList<String>(Arrays.asList("1", "2", "3", "4"));
        map1 = new HashMap<String, Integer>();
        map1.put("1", 1);
        map1.put("2", 2);
        map1.put("3", 3);

        strel1 = "el1";
        strel2 = "el2";
        strel3 = "el3";
        strel4 = "el4";
        strel5 = "el5";
        strel6 = "el6";
        strel7 = "el7";
        intel1 = 1;
        intel2 = 2;
        intel3 = 3;
        intel4 = 4;
        intel5 = 5;
        intel6 = 6;
        intel7 = 7;
    }

    public String getStrel1() {
        return strel1;
    }

    public void setStrel1(String strel1) {
        this.strel1 = strel1;
    }

    public String getStrel2() {
        return strel2;
    }

    public void setStrel2(String strel2) {
        this.strel2 = strel2;
    }

    public String getStrel3() {
        return strel3;
    }

    public void setStrel3(String strel3) {
        this.strel3 = strel3;
    }

    public String getStrel4() {
        return strel4;
    }

    public void setStrel4(String strel4) {
        this.strel4 = strel4;
    }

    public String getStrel5() {
        return strel5;
    }

    public void setStrel5(String strel5) {
        this.strel5 = strel5;
    }

    public String getStrel6() {
        return strel6;
    }

    public void setStrel6(String strel6) {
        this.strel6 = strel6;
    }

    public String getStrel7() {
        return strel7;
    }

    public void setStrel7(String strel7) {
        this.strel7 = strel7;
    }

    public Integer getIntel1() {
        return intel1;
    }

    public void setIntel1(Integer intel1) {
        this.intel1 = intel1;
    }

    public Integer getIntel2() {
        return intel2;
    }

    public void setIntel2(Integer intel2) {
        this.intel2 = intel2;
    }

    public Integer getIntel3() {
        return intel3;
    }

    public void setIntel3(Integer intel3) {
        this.intel3 = intel3;
    }

    public Integer getIntel4() {
        return intel4;
    }

    public void setIntel4(Integer intel4) {
        this.intel4 = intel4;
    }

    public Integer getIntel5() {
        return intel5;
    }

    public void setIntel5(Integer intel5) {
        this.intel5 = intel5;
    }

    public Integer getIntel6() {
        return intel6;
    }

    public void setIntel6(Integer intel6) {
        this.intel6 = intel6;
    }

    public Integer getIntel7() {
        return intel7;
    }

    public void setIntel7(Integer intel7) {
        this.intel7 = intel7;
    }

    public ArrayList<Integer> getList1() {
        return list1;
    }

    public void setList1(ArrayList<Integer> list1) {
        this.list1 = list1;
    }

    public List<String> getList2() {
        return list2;
    }

    public void setList2(List<String> list2) {
        this.list2 = list2;
    }

    public Map<String, Integer> getMap1() {
        return map1;
    }

    public void setMap1(Map<String, Integer> map1) {
        this.map1 = map1;
    }
}
