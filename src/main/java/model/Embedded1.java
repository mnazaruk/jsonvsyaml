package model;

import java.util.*;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public class Embedded1 {
    public String strel1;
    public String strel2;
    public String strel3;
    public Integer intel1;
    public Integer intel2;
    public Integer intel3;

    public ArrayList<Integer> list1;
    public List<Integer> list2;
    public List<String> list3;
    public Map<String, Integer> map1;

    public Embedded1() {
    }

    public Embedded1(boolean flag) {
        map1  = new HashMap<String, Integer>();
        map1.put("1", 1);
        map1.put("2", 2);
        map1.put("3", 3);
        list1 = new ArrayList<Integer>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        list1.add(5);
        list3 = new ArrayList<String>(Arrays.asList("1", "2", "3", "4"));
        list2 = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4));
        strel1 = "el1";
        strel2 = "el2";
        strel3 = "el3";
        intel1 = 1;
        intel2 = 2;
        intel3 = 3;
    }
}
