package validation;

import model.Author;
import model.SimpleBook;
import org.jruby.RubyProcess;
import parsers.SimpleBookYamlParser;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;
import java.util.Set;

public class TryBean {

    public static void main(String[] args) {

        String filename = "yaml_files/validation/book.yml";
        //createBook(filename);


        SimpleBookYamlParser parser = new SimpleBookYamlParser(filename);
        try {
            SimpleBook book = parser.deserialize();

            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.getValidator();
            Set<ConstraintViolation<SimpleBook>> violations = validator.validate(book);
            for (ConstraintViolation<SimpleBook> violation : violations) {
                System.out.println(violation.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void createBook(String filename){
        Author author = new Author("Harry Potter", "9379992");
        SimpleBook simpleBook = new SimpleBook("Diary", author, new Date());

        SimpleBookYamlParser parser = new SimpleBookYamlParser(filename);
        parser.serialize(simpleBook);
    }
}
