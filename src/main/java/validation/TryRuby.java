package validation;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class TryRuby {
    public static void main(String[] args) {
        System.setProperty("org.jruby.embed.compat.version", "JRuby1.9");
        ScriptEngineManager factory = new ScriptEngineManager();

        ScriptEngine scriptEngine = factory.getEngineByName("jruby");

        try {
            scriptEngine.eval("require 'validation'");
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }
}