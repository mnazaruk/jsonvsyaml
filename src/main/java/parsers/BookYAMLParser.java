package parsers;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import model.Book;
import org.jvyaml.YAML;
import org.jvyaml.YAMLConfig;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public class BookYAMLParser implements Parser<Book> {
    String filename;

    public BookYAMLParser(String filename) {
        this.filename = filename;
    }

    @Override
    public void serialize(Book book) {

        try {
            YamlWriter writer = new YamlWriter(new FileWriter(filename));
            writer.write(book);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void serialize1(Book book) {

        try {
            DumperOptions options = new DumperOptions();
            options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
            Yaml yaml = new Yaml(options);
            FileWriter writer = new FileWriter(filename);
            yaml.dump(book, writer);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void serialize2(Book book) {

        try {
            org.ho.yaml.Yaml.dump(book, new File(filename), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Book deserialize() throws Exception {
        try {
            YamlReader reader = new YamlReader(new FileReader(filename));
            Book book = reader.read(Book.class);
            return book;
            //return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (YamlException e) {
            String message = "Exception in file " + filename + ", ";
            throw new Exception(message + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Book deserialize1() throws Exception {
        try {
            InputStream input = new FileInputStream(new File(filename));
            Yaml yaml = new Yaml();
            Book data = (Book) yaml.load(input);
            input.close();

            return data;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (YamlException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            String message = "Exception in file " + filename + ", ";
            throw new Exception(message + e.getMessage());
        }
        return null;
    }

    public Book deserialize2() throws Exception {
        try {
            Book data = org.ho.yaml.Yaml.loadType(new File(filename), Book.class);
            return data;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            String message = "Exception in file " + filename + ", ";
            throw new Exception(message + e.getMessage());
        }
        return null;
    }
}
