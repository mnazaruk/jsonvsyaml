package parsers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import model.Book;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public class BookJSONParser implements Parser<Book> {

    String filename;
    public BookJSONParser(String filename) {
        this.filename = filename;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public void serialize(Book book) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();;

        try {
            FileWriter writer = new FileWriter(filename);
            String json = gson.toJson(book);
            writer.write(json);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Book deserialize() {
        Gson gson = new Gson();

        try {
            BufferedReader br = new BufferedReader(
                    new FileReader(filename));

            JsonReader jsonReader = new JsonReader(br);
            Book book = gson.fromJson(jsonReader, Book.class);
            return book;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
