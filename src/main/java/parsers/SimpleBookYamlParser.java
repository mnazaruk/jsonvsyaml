package parsers;

import com.esotericsoftware.yamlbeans.YamlException;
import model.SimpleBook;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

public class SimpleBookYamlParser implements Parser<SimpleBook> {

    String filename;

    public SimpleBookYamlParser() {
    }

    public SimpleBookYamlParser(String filename) {
        this.filename = filename;
    }

    @Override
    public void serialize(SimpleBook book) {
        try {
            DumperOptions options = new DumperOptions();
            options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
            Yaml yaml = new Yaml(options);
            FileWriter writer = new FileWriter(filename);
            yaml.dump(book, writer);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public SimpleBook deserialize() throws Exception {
        try {
            InputStream input = new FileInputStream(new File(filename));
            Yaml yaml = new Yaml();
            SimpleBook data = (SimpleBook) yaml.load(input);
            input.close();

            return data;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            String message = "Exception in file " + filename + ", ";
            throw new Exception(message + e.getMessage());
        }
        return null;
    }

    @Override
    public String getFilename() {
        return null;
    }

    @Override
    public void setFilename(String filename) {

    }
}
