package parsers;

import java.util.ArrayList;

/**
 * Created by matveinazaruk on 8/22/14.
 */
public interface Parser <T> {
    public void serialize(T to);
    public T deserialize() throws Exception;
    public String getFilename();
    public void setFilename(String filename);
}
