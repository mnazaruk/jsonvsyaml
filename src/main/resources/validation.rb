##require 'yaml'
##begin
##    YAML.load_file('document01a.yaml')
##rescue => e
##    puts "#{(e.message.match(/on line (\d+)/)[1] + ':') rescue nil} #{e.message}"
##end

require 'kwalify'

## load schema data
schema = Kwalify::Yaml.load_file('yaml_files/validation/book-scheme.yml')

## create validator
validator = Kwalify::Validator.new(schema)

## load document
document = Kwalify::Yaml.load_file('yaml_files/validation/book.yml')

## validate
errors = validator.validate(document)

## show errors
if errors && !errors.empty?
  for e in errors
    puts "[#{e.path}] #{e.message}"
  end
end


